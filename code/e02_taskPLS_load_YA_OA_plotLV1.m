% Create an overview plot featuring the results of the multivariate PLS
% comparing spectral changes during the stimulus period under load

clear all; cla; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(genpath(fullfile(pn.tools, '[MEG]PLS', 'MEGPLS_PIPELINE_v2.02b')))
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(fullfile(pn.tools, 'RainCloudPlots'));

% set custom colormap
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)

load(fullfile(pn.data, 'D_taskPLS_2group.mat'), ...
    'stat', 'result', 'lvdat', 'lv_evt_list')

indLV = 3;

lvdat = reshape(result.boot_result.compare_u(:,indLV), 60, 1, 1);
stat.prob = lvdat;
stat.mask = lvdat > 2 | lvdat < -2;

%% invert solution

% stat.mask = stat.mask;
% stat.prob = stat.prob.*-1;
% result.vsc = result.vsc.*-1;
% result.usc = result.usc.*-1;

h = figure('units','normalized','position',[.1 .1 .3 .3]);
set(gcf,'renderer','Painters')

% plot multivariate brainscores

maskNaN = double(stat.mask);
maskNaN(maskNaN==0) = NaN;

% plot multivariate topographies

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/B_data/elec.mat')

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colormap = cBrew;
cfg.colorbar = 'EastOutside';

plotData = [];
plotData.label = elec.label; % {1 x N}
plotData.dimord = 'chan';
subplot(1,1,1); cla;
    cfg.marker = 'off'; 
    cfg.highlight = 'yes';
    cfg.highlightchannel = plotData.label(58:60);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    %cfg.zlim = [-10 10]; 
    plotData.powspctrm = squeeze(stat.mask.*stat.prob); ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','BSR');
colormap(cBrew)

set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% plot using raincloud plot

    groupsizes=result.num_subj_lst;
    conditions=lv_evt_list;
    conds = {'Rest'; 'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
    condData = []; uData = [];
    for indGroup = 1:2
        if indGroup == 1
            relevantEntries = 1:groupsizes(1)*numel(conds);
        elseif indGroup == 2
            relevantEntries = groupsizes(1)*numel(conds)+1:...
                 groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
        end
        for indCond = 1:numel(conds)
            targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
            condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
            uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
        end
    end
    
    cBrew = brewermap(4,'RdBu');
    cBrew = cBrew([1,4],:);
    
    cBrew(1,:) = 2.*[.3 .1 .1];
    cBrew(2,:) = 2.*[.1 .1 .3];

    %% plot RainCloudPlot (within-subject centered)

    h = figure('units','normalized','position',[.1 .1 .3 .2]);
    for indGroup = 1:2
        subplot(1,2,indGroup)
        set(gcf,'renderer','Painters')
        curData = uData{indGroup}';
        % read into cell array of the appropriate dimensions
        data = []; data_ws = [];
        for i = 1:5
            for j = 1:1
                data{i, j} = squeeze(curData(:,i));
                % individually demean for within-subject visualization
                data_ws{i, j} = curData(:,i)-...
                    nanmean(curData(:,:),2)+...
                    repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
            end
        end
        % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!
        cl = cBrew(indGroup,:);
        box off
        hold on;
        h_rc = rm_raincloud(data_ws, cl,1);
        view([90 -90]);
        axis ij
        box(gca,'off')
        set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'; 'Rest2'; 'Rest1'});
        ylabel('Target load'); xlabel({'Brainscore (a.u.)'})
        set(findall(gcf,'-property','FontSize'),'FontSize',20)
        %xlim([110 150])
        curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(5)+.5*(curYTick(2)-curYTick(1))]);
        % assess linear effect
        curData = [data{1, 1}, data{2, 1}, data{3, 1}, data{4, 1},data{5, 1}];
        X = [1 1; 1 2; 1 3; 1 4 ; 1 5]; b=X\curData'; IndividualSlopes = b(2,:);
        [~, p, ci, stats] = ttest(IndividualSlopes);
        legend(['M:', num2str(mean(IndividualSlopes)), '; p=', num2str(p)], 'location', 'SouthEast')
    end
    suptitle('ws-centered')
