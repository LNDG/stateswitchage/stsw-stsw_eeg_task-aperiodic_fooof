%% setup 

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.figures      = fullfile(rootpath, 'figures');
pn.fooof        = fullfile(pn.data, 'B_1_f_fooof');
pn.fooof_rest   = fullfile(rootpath, '..', '..', 'stsw_eeg_rest', 'G_fooof', 'data');
pn.tools        = fullfile(rootpath, '..', 'aperiodic', 'tools');
    addpath(pn.tools);
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(fullfile(pn.tools, 'BrewerMap'));
    addpath(fullfile(pn.tools, 'shadedErrorBar'));
    addpath(fullfile(pn.tools, 'barwitherr'));
    addpath(genpath(fullfile(pn.tools, 'RainCloudPlots')));
    addpath(fullfile(pn.tools, 'Cookdist'));
pn.summary	= fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');

%% load data

load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')
load(fullfile(pn.data, 'foof_exponents.mat'), 'fooof_exponents_rest', 'fooof_exponents')

%% plot parametric 1/f slope modulation

% N = 47;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

summaryIdx = ismember(STSWD_summary.IDs, IDs);

slopeFit = squeeze(nanmean(fooof_exponents(summaryIdx,:,58:60),3));

dataToPlot = slopeFit;

% define outlier as lin. modulation of 2.5*mean Cook's distance
cooks = Cookdist(dataToPlot(:,:));
outliers = cooks>2.5*mean(cooks);
idx_outlier = find(outliers);
idx_standard = find(outliers==0);

% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1:4
    for j = 1:1
        data{i, j} = dataToPlot(:,i);
        % individually demean for within-subject visualization
        data_ws{i, j} = dataToPlot(:,i)-...
            nanmean(dataToPlot(:,:),2)+...
            repmat(nanmean(nanmean(dataToPlot(:,:),2),1),size(dataToPlot(:,:),1),1);
        data_nooutlier{i, j} = data{i, j};
        data_nooutlier{i, j}(idx_outlier) = [];
        data_ws_nooutlier{i, j} = data_ws{i, j};
        data_ws_nooutlier{i, j}(idx_outlier) = [];
        % sort outliers to back in original data for improved plot overlap
        data_ws{i, j} = [data_ws{i, j}(idx_standard); data_ws{i, j}(idx_outlier)];
    end
end

% IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

cl = [.6 .2 .2];

h = figure('units','normalized','position',[.1 .1 .2 .3]);
set(gcf,'renderer','Painters')
    cla;
    rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1,[],[],[],15);
    h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1,[],[],[],15);
    % add stats
%         condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
%         condPairsLevel = [.4, .39, .38, .37, .36, .345];
    condPairs = [1,3; 2,4];
    condPairsLevel = [.48, .49];
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
           mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

        end
    end
    view([90 -90]);
    axis ij
box(gca,'off')
%set(gca, 'YTick', [1,2,3,4]);
set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
ylabel('Target load'); xlabel({'slope'; '[Individually centered]'})
set(findall(gcf,'-property','FontSize'),'FontSize',30)
yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./2, yticks(4)+(yticks(2)-yticks(1))./2]);
minmax = [min(min(cat(2,data_ws{:}))), max(max(cat(2,data_ws{:})))];
xlim(minmax+[-0.2*diff(minmax), 0.2*diff(minmax)])

% test linear effect
curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
[~, p, ci, stats] = ttest(IndividualSlopes);
title(['M:', num2str(round(mean(IndividualSlopes),3)), '; p=', num2str(round(p,3))])

figureName = 'd_fooof_rcp_ya';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% correlate with existing linear fit values

% figure; 
% subplot(1,2,1);
% x = squeeze(nanmean(slopeFit(summaryIdx,:),2));
% y = squeeze(nanmean(STSWD_summary.OneFslope.data(summaryIdx,:),2));
% scatter(x,y, 'filled')
% subplot(1,2,2);
% X = [1 1; 1 2; 1 3; 1 4];
% b=X\slopeFit';
% slopeFit_lin = b(2,summaryIdx);
% x = slopeFit_lin;
% y = STSWD_summary.OneFslope.linear(summaryIdx);
% scatter(x,y, 'filled')

%% plot for OAs

IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
'2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
'2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
'2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
'2252';'2258';'2261'};
summaryIdx = ismember(STSWD_summary.IDs, IDs);  

slopeFit = squeeze(nanmean(fooof_exponents(summaryIdx,:,58:60),3));

dataToPlot = slopeFit;

% define outlier as lin. modulation of 2.5*mean Cook's distance
cooks = Cookdist(dataToPlot(:,:));
outliers = cooks>2.5*mean(cooks);
idx_outlier = find(outliers);
idx_standard = find(outliers==0);

% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1:4
    for j = 1:1
        data{i, j} = dataToPlot(:,i);
        % individually demean for within-subject visualization
        data_ws{i, j} = dataToPlot(:,i)-...
            nanmean(dataToPlot(:,:),2)+...
            repmat(nanmean(nanmean(dataToPlot(:,:),2),1),size(dataToPlot(:,:),1),1);
        data_nooutlier{i, j} = data{i, j};
        data_nooutlier{i, j}(idx_outlier) = [];
        data_ws_nooutlier{i, j} = data_ws{i, j};
        data_ws_nooutlier{i, j}(idx_outlier) = [];
        % sort outliers to back in original data for improved plot overlap
        data_ws{i, j} = [data_ws{i, j}(idx_standard); data_ws{i, j}(idx_outlier)];
    end
end

% IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

cl = [.6 .2 .2];

h = figure('units','normalized','position',[.1 .1 .2 .3]);
set(gcf,'renderer','Painters')
    cla;
    rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1,[],[],[],15);
    h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1,[],[],[],15);
    % add stats
%         condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
%         condPairsLevel = [.4, .39, .38, .37, .36, .345];
    condPairs = [1,3; 2,4];
    condPairsLevel = [.48, .49];
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
           mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

        end
    end
    view([90 -90]);
    axis ij
box(gca,'off')
%set(gca, 'YTick', [1,2,3,4]);
set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
ylabel('Target load'); xlabel({'slope'; '[Individually centered]'})
set(findall(gcf,'-property','FontSize'),'FontSize',30)
yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./2, yticks(4)+(yticks(2)-yticks(1))./2]);
minmax = [min(min(cat(2,data_ws{:}))), max(max(cat(2,data_ws{:})))];
xlim(minmax+[-0.2*diff(minmax), 0.2*diff(minmax)])

% test linear effect
curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
[~, p, ci, stats] = ttest(IndividualSlopes);
title(['M:', num2str(round(mean(IndividualSlopes),3)), '; p=', num2str(round(p,3))])

figureName = 'd_fooof_rcp_oa';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% correlate with existing linear fit values

% figure; 
% subplot(1,2,1);
% x = squeeze(nanmean(slopeFit(summaryIdx,:),2));
% y = squeeze(nanmean(STSWD_summary.OneFslope.data(summaryIdx,:),2));
% scatter(x,y, 'filled')
% subplot(1,2,2);
% X = [1 1; 1 2; 1 3; 1 4];
% b=X\slopeFit';
% slopeFit_lin = b(2,summaryIdx);
% x = slopeFit_lin;
% y = STSWD_summary.OneFslope.linear(summaryIdx);
% scatter(x,y, 'filled')
