# coding: utf-8

# import sys
#!{sys.executable} -m pip install fooof
import numpy as np
import os as os
from scipy.io import loadmat, savemat
from fooof import FOOOFGroup
import sys

print(sys.argv[0])  # returns scriptname
print(sys.argv[1])  # returns rootdir
print(sys.argv[2])  # returns ID

 # Set up current paths etc.
print("Setting up paths ...\n")
rootdir = sys.argv[1]
curSub = sys.argv[2]
datadir = os.path.join(rootdir, 'data','B_1_f_fooof')
if not os.path.isdir(datadir):
    os.mkdir(datadir)

# Load the mat file
print("Loading backgrounds from .mat ...\n")
filename = os.path.join(datadir, curSub + '_1f_fooof.mat')
dat = loadmat(filename)

# Unpack data from dictionary, and squeeze into numpy arrays
psds_3d = np.squeeze(dat['freq']['powspctrm'][0][0]).astype('float')
# choose only the first condition here
for condition_idx in range(psds_3d.shape[0]):
    psds = psds_3d[condition_idx, :, :]
    freqs = np.squeeze(dat['freq']['freq'][0][0]).astype('float')
    # ^Note: this also explicitly enforces type as float (type casts to float64, instead of float32)
    #  This is not strictly necessary for fitting, but is for saving out as json from FOOOF, if you want to do that
    # FOOOF expects psds to be nChan*nFreq, no preference for 1D freqs

    # Initialize FOOOF object
    fg = FOOOFGroup()

    # Fit the FOOOF model on all PSDs, and report
    fg.report(freqs, psds, [2, 80])

    # for channel in range(len(fg.get_params('error'))):
    #     fm = fg.get_fooof(channel, regenerate=True)
    #     fm.plot()

    # Save out a specific FOOOF measure of interest - for example, slopes
    exps = fg.get_params('aperiodic_params', 'exponent')
    savemat(os.path.join(datadir, 'exps'+'_'+curSub+'_' +
            str(condition_idx+1)+'.mat'), {'exps': exps})

    # Save out fooof results to json file
    #  There is a utility file to load this json file directly into Matlab
    fg.save(os.path.join(datadir, 'fooof_results'+'_'+curSub+'_' +
            str(condition_idx+1)), save_results=True)

print(curSub+'Done!')
