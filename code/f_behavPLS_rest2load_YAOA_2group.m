% Build a two age-group model, probing joint uncertainty-related changes in
% spectral power, pupil dilation, 1/f exponents, sample entropy

clear all; cla; clc;
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/T_tools/[MEG]PLS/MEGPLS_PIPELINE_v2.02b'))

%% add seed for reproducibility

rng(0, 'twister');

%% main PLS part

% load data and prepare as PLS input

pn.summarydata = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.summarydata, 'STSWD_summary_YAOA.mat'], 'STSWD_summary')

%% load data

IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%2139, 2227 removed (anatomic anomalies)
IDs{2} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

num_chans = 60;
num_freqs = 1;
num_time = 1;

num_subj_lst = [numel(IDs{1}), numel(IDs{2})];
num_cond = 1;
num_grp = 2;

input_data = cell(num_grp,1);
for indGroup = 1:2
    idx_ids = ismember(STSWD_summary.IDs, IDs{indGroup});
    input_data{indGroup} = squeeze(nanmean(STSWD_summary.fooof.OneFslope_rest.data(idx_ids,2,:),2));
end

datamat_lst = cell(num_grp,1); lv_evt_list = [];
indCount_cont = 1;
for indGroup = 1:num_grp
    indCount = 1;
    curTFRdata = input_data{indGroup};
    for indCond = 1:num_cond
        for indID = 1:num_subj_lst(indGroup)
            datamat_lst{indGroup}(indCount,:) = reshape(squeeze(curTFRdata(indID,:)), [], 1);
            lv_evt_list(indCount_cont) = indCond;
            indCount = indCount+1;
            indCount_cont = indCount_cont+1;
        end
    end
end
datamat_lst{indGroup}(isnan(datamat_lst{indGroup})) = 0;

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary_YAOA.mat')

stacked_behavdata = [];
indCount = 1;
for indGroup = 1:num_grp
    for indCond = 1:num_cond
        for indID = 1:num_subj_lst(indGroup)
            curID = ismember(STSWD_summary.IDs,IDs{indGroup}(indID));
            stacked_behavdata(indCount,1) = nanmean(STSWD_summary.fooof.OneFslope.linear(curID,1),2);
            stacked_behavdata(indCount,2) = nanmean(STSWD_summary.HDDM_vt.driftEEG(curID,1),2);
            stacked_behavdata(indCount,3) = nanmean(STSWD_summary.HDDM_vt.driftEEG_linear(curID,1),2);
            indCount = indCount+1;
        end
    end
end

stacked_behavdata = zscore(stacked_behavdata,[],1);

%figure; scatter(stacked_behavdata(:,1), stacked_behavdata(:,2))

%% set PLS options and run PLS

option = [];
option.method = 3; % [1] | 2 | 3 | 4 | 5 | 6
option.num_perm = 1000; %( single non-negative integer )
option.num_split = 0; %( single non-negative integer )
option.num_boot = 1000; % ( single non-negative integer )
option.stacked_behavdata = stacked_behavdata; %( 2-D numerical matrix )
option.cormode = 0; % [0] | 2 | 4 | 6
option.boot_type = 'strat'; %['strat'] | 'nonstrat'

result = pls_analysis(datamat_lst, num_subj_lst, num_cond, option);

%% rearrange into fieldtrip structure

lvdat = reshape(result.boot_result.compare_u(:,1), num_chans, num_freqs, num_time);
%udat = reshape(result.u, num_chans, num_freqs, num_time);

stat = [];
stat.prob = lvdat;
stat.dimord = 'chan_freq_time';
stat.clusters = [];
stat.clusters.prob = result.perm_result.sprob; % check for significance of LV
stat.mask = lvdat > 3 | lvdat < -3;
stat.cfg = option;

%save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S18_1_f/B_data/D_taskPLS_2group.mat', 'stat', 'result', 'lvdat', 'lv_evt_list')

%%

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/B_data/elec.mat')

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colormap = cBrew;
cfg.colorbar = 'EastOutside';

plotData = [];
plotData.label = elec.label; % {1 x N}
plotData.dimord = 'chan';
figure; cla;
    cfg.marker = 'off'; 
    cfg.zlim = [-3 3]; 
    plotData.powspctrm = stat.prob; ft_topoplotER(cfg,plotData);
    cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Max BSR');