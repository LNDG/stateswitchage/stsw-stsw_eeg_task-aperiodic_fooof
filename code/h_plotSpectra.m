%% set paths

restoredefaultpath;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.dataOut      = fullfile(rootpath, 'data', 'B_1_f_fooof');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, '..', 'aperiodic', 'tools');
    addpath(pn.tools);
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(fullfile(pn.tools, 'BrewerMap'));
    addpath(fullfile(pn.tools, 'shadedErrorBar'));

%% load PSD data

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

MergedPow = [];
for id = 1:length(IDs)
    load(fullfile(pn.dataOut, [IDs{id}, '_1f_fooof.mat']), 'freq');
    MergedPow(id,:,:,:) = freq.powspctrm;
end

%% plot occipital spectra YA

idx_chan = 58:60;
idx_ids = ageIdx{1};

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);
h = figure('units','centimeter','position',[0 0 35 9]);
subplot(1,3,1); hold on;
    condAvg = squeeze(nanmean(nanmean(log10(MergedPow(idx_ids,:,idx_chan,:)),3),2));
    curData = squeeze(nanmean(log10(MergedPow(idx_ids,1,idx_chan,:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(log10(MergedPow(idx_ids,4,idx_chan,:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
    xlim([0 1.9]); %ylim([-10 -6.5])
    xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
    %legend([l1.mainLine, l4.mainLine], {'1 Target', '4 Targets'}, 'location', 'SouthWest'); legend('boxoff')
subplot(1,3,2); hold on;
    condAvg = squeeze(nanmean(nanmean(log10(MergedPow(idx_ids,:,idx_chan,:)),3),2));
    for indCond = 1:4
        curData = squeeze(nanmean(log10(MergedPow(idx_ids,indCond,idx_chan,:)),3));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        lline{indCond} = plot(log10(freq.freq),nanmean(curData,1), ...
            'color', cBrew(indCond,:),'linewidth', 3);
    end
    xlim([0 0.2]); ylim([-7.5 -7.1])
%     legend([lline{1}, lline{2}, lline{3}, lline{4}], ...
%         {'1', '2', '3', '4'}, 'location', 'SouthWest'); legend('boxoff')
    xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
subplot(1,3,3); hold on;
    condAvg = squeeze(nanmean(nanmean(log10(MergedPow(idx_ids,:,idx_chan,:)),3),2));
    for indCond = 1:4
        curData = squeeze(nanmean(log10(MergedPow(idx_ids,indCond,idx_chan,:)),3));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        lline{indCond} = plot(log10(freq.freq),nanmean(curData,1), ...
            'color', cBrew(indCond,:),'linewidth', 3);
    end
    xlim([1.5 1.9]); ylim([-9 -8.7])
    legend([lline{1}, lline{2}, lline{3}, lline{4}], ...
        {'1', '2', '3', '4'}, 'location', 'SouthWest'); legend('boxoff')
    xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');

set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['h_spectra_ya'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot occipital spectra OA

idx_chan = 58:60;
idx_ids = ageIdx{2};

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);
h = figure('units','centimeter','position',[0 0 35 9]);
subplot(1,3,1); hold on;
    condAvg = squeeze(nanmean(nanmean(log10(MergedPow(idx_ids,:,idx_chan,:)),3),2));
    curData = squeeze(nanmean(log10(MergedPow(idx_ids,1,idx_chan,:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(log10(MergedPow(idx_ids,4,idx_chan,:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
    xlim([0 1.9]); %ylim([-10 -6.5])
    xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
    %legend([l1.mainLine, l4.mainLine], {'1 Target', '4 Targets'}, 'location', 'SouthWest'); legend('boxoff')
subplot(1,3,2); hold on;
    condAvg = squeeze(nanmean(nanmean(log10(MergedPow(idx_ids,:,idx_chan,:)),3),2));
    for indCond = 1:4
        curData = squeeze(nanmean(log10(MergedPow(idx_ids,indCond,idx_chan,:)),3));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        lline{indCond} = plot(log10(freq.freq),nanmean(curData,1), ...
            'color', cBrew(indCond,:),'linewidth', 3);
    end
    xlim([0 0.2]); ylim([-7.6 -7.3])
    xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
subplot(1,3,3); hold on;
    condAvg = squeeze(nanmean(nanmean(log10(MergedPow(idx_ids,:,idx_chan,:)),3),2));
    for indCond = 1:4
        curData = squeeze(nanmean(log10(MergedPow(idx_ids,indCond,idx_chan,:)),3));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        lline{indCond} = plot(log10(freq.freq),nanmean(curData,1), ...
            'color', cBrew(indCond,:),'linewidth', 3);
    end
    xlim([1.5 1.9]); ylim([-9.2 -8.9])
    legend([lline{1}, lline{2}, lline{3}, lline{4}], ...
        {'1', '2', '3', '4'}, 'location', 'SouthWest'); legend('boxoff')
    xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');

set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['h_spectra_oa'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% check individual spectra

idx_chan = 58:60;

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);
h = figure('units','centimeter','position',[0 0 30 30]);
for indID = 1:numel(ageIdx{1})
    h = figure('units','centimeter','position',[0 0 30 30]);
    hold on;
    curData = squeeze(nanmean(log10(MergedPow(indID,1,idx_chan,:)),3));
    plot(log10(freq.freq),curData, 'linewidth', 2);
    curData = squeeze(nanmean(log10(MergedPow(indID,4,idx_chan,:)),3));
    plot(log10(freq.freq),curData, 'linewidth', 2);
    xlim([0 1.9]); %ylim([-10 -6.5])
    xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
    title(IDs{indID});
    figureName = ['individualspectra_',IDs{indID}];
    path = fullfile(pn.figures, 'tmp'); mkdir(path);
    saveas(h, fullfile(path, figureName), 'png');
    close(h);
end

%% plot frontal spectrum 

% cBrew = brewermap(4,'RdBu');
% cBrew = flipud(cBrew);
% h = figure('units','normalized','position',[.1 .1 .125 .2]); hold on;
% curData = squeeze(nanmean(nanmean(log10(MergedPow(:,:,10:12,:)),3),2));
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l1 = shadedErrorBar([],nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25)
% set(gca, 'XTick', [1:4:28]);
% xlim([1 24]); set(gca, 'XTickLabels', round(freq.freq(get(gca, 'XTick')),1));
% xlabel('Frequency (Hz)'); ylabel('Power (log10; Hz)');
% set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% check whether there is frontal 1/f rotation

% load(fullfile(pn.data, 'g01_taskPLS_2group.mat'));
% 
% indLV = 2;
% lvdat = reshape(result.boot_result.compare_u(:,indLV), 60, 1, 1);
% stat.prob = lvdat;
% stat.mask = lvdat > 2 | lvdat < -2;
% 
% idx_chan = find(stat.mask(1:30));

idx_chan = 12;

% shading presents within-subject standard errors
% new value = old value ??? subject average + grand average

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);
h = figure('units','normalized','position',[.1 .1 .1 .2]); hold on;
condAvg = squeeze(nanmean(nanmean(log10(MergedPow(:,:,idx_chan,:)),3),2));
curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[1,4],idx_chan,:)),3),2));
curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
% curData = squeeze(nanmean(log10(MergedPow(:,2,idx_chan,:)),3));
% curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
% standError = nanstd(curData,1)./sqrt(size(curData,1));
% l2 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%     'lineprops', {'color', cBrew(3,:),'linewidth', 2}, 'patchSaturation', .25);
curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[2,3],idx_chan,:)),3),2));
curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l3 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
xlim([0 1.9]); %ylim([-10 -6.5])
xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
legend([l1.mainLine, l3.mainLine], {'1 Target', '3 Targets'}, 'location', 'SouthEast'); legend('boxoff')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);
h = figure('units','normalized','position',[.1 .1 .1 .2]); hold on;
curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[1,4],idx_chan,:)),3),2))-squeeze(nanmean(nanmean(log10(MergedPow(:,[2,3],idx_chan,:)),3),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);
h = figure('units','normalized','position',[.1 .1 .1 .2]); hold on;
curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[1],idx_chan,:)),3),2))-squeeze(nanmean(nanmean(log10(MergedPow(:,[1:4],idx_chan,:)),3),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[2],idx_chan,:)),3),2))-squeeze(nanmean(nanmean(log10(MergedPow(:,[1:4],idx_chan,:)),3),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);

curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[3],idx_chan,:)),3),2))-squeeze(nanmean(nanmean(log10(MergedPow(:,[1:4],idx_chan,:)),3),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);


curData = squeeze(nanmean(nanmean(log10(MergedPow(:,[4],idx_chan,:)),3),2))-squeeze(nanmean(nanmean(log10(MergedPow(:,[1:4],idx_chan,:)),3),2));
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
    'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);


%% plot frontal spectrum (YA only 1 vs 3)

idx_chan = 12;
idx_ids = ageIdx{1};

cBrew = brewermap(4,'RdBu');
cBrew = flipud(cBrew);
h = figure('units','centimeter','position',[0 0 35 9]);
subplot(1,3,1); hold on;
    condAvg = squeeze(nanmean(nanmean(log10(MergedPow(idx_ids,:,idx_chan,:)),3),2));
    curData = squeeze(nanmean(log10(MergedPow(idx_ids,1,idx_chan,:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(log10(MergedPow(idx_ids,3,idx_chan,:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
    xlim([0 1.9]); %ylim([-10 -6.5])
    xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
    %legend([l1.mainLine, l4.mainLine], {'1 Target', '4 Targets'}, 'location', 'SouthWest'); legend('boxoff')
subplot(1,3,2); hold on;
    condAvg = squeeze(nanmean(nanmean(log10(MergedPow(idx_ids,:,idx_chan,:)),3),2));
    for indCond = 1:4
        curData = squeeze(nanmean(log10(MergedPow(idx_ids,indCond,idx_chan,:)),3));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        lline{indCond} = plot(log10(freq.freq),nanmean(curData,1), ...
            'color', cBrew(indCond,:),'linewidth', 3);
    end
    xlim([0 0.2]); ylim([-7.6 -7.3])
    xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
subplot(1,3,3); hold on;
    condAvg = squeeze(nanmean(nanmean(log10(MergedPow(idx_ids,:,idx_chan,:)),3),2));
    for indCond = 1:4
        curData = squeeze(nanmean(log10(MergedPow(idx_ids,indCond,idx_chan,:)),3));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        lline{indCond} = plot(log10(freq.freq),nanmean(curData,1), ...
            'color', cBrew(indCond,:),'linewidth', 3);
    end
    xlim([1.5 1.9]); ylim([-9.2 -8.9])
    legend([lline{1}, lline{2}, lline{3}, lline{4}], ...
        {'1', '2', '3', '4'}, 'location', 'SouthWest'); legend('boxoff')
    xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
