%% setup 

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.fooof        = fullfile(pn.data, 'B_1_f_fooof');
pn.fooof_rest   = fullfile(rootpath, '..', '..', 'stsw_eeg_rest', 'fooof', 'data');
pn.tools        = fullfile(rootpath, '..', 'aperiodic', 'tools');
    addpath(pn.tools);
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(fullfile(pn.tools, 'BrewerMap'));
    addpath(fullfile(pn.tools, 'shadedErrorBar'));
    addpath(fullfile(pn.tools, 'barwitherr'));
    addpath(fullfile(pn.tools, 'RainCloudPlots'));
pn.summary	= fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');

%% load data

load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')

fooof_exponents = [];
for indID = 1:numel(STSWD_summary.IDs)
    for indLoad = 1:4
        try
            fooof_in = load(fullfile(pn.fooof, ['exps_', STSWD_summary.IDs{indID} , '_',num2str(indLoad),'.mat']));
            fooof_exponents(indID,indLoad,:) = -1.*fooof_in.exps; % note: get negative exponent
        catch
            fooof_exponents(indID,indLoad,:) = NaN;
            disp(['Error: ', num2str(indID), ' ', num2str(indLoad)]);
        end
    end
end

% interpolate within-person NaN values (avg. across other loads)
[idx_xyz] = find(isnan(fooof_exponents));
[idx_x, idx_y, idx_z] = ind2sub(size(fooof_exponents),idx_xyz);
for ind_nan = 1:numel(idx_x)
    fooof_exponents(idx_x(ind_nan), idx_y(ind_nan), idx_z(ind_nan)) = ...
        nanmean(fooof_exponents(idx_x(ind_nan), :, idx_z(ind_nan)),2);
end
clear idx_xyz idx_x idx_y idx_z

%% load resting state spectral exponents

fooof_exponents_rest = [];
for indID = 1:numel(STSWD_summary.IDs)
    for indLoad = 1:2
        try
            fooof_in = load(fullfile(pn.fooof_rest,['exps_', STSWD_summary.IDs{indID} , '_',num2str(indLoad),'.mat']));
            fooof_exponents_rest(indID,indLoad,:) = -1.*fooof_in.exps; % note: get negative exponent
        catch
            fooof_exponents_rest(indID,indLoad,:) = NaN;
            disp(['Error: ', num2str(indID), ' ', num2str(indLoad)]);
        end
    end
end

% interpolate within-person NaN values (avg. across other loads)
[idx_xyz] = find(isnan(fooof_exponents_rest));
[idx_x, idx_y, idx_z] = ind2sub(size(fooof_exponents_rest),idx_xyz);
for ind_nan = 1:numel(idx_x)
    fooof_exponents_rest(idx_x(ind_nan), idx_y(ind_nan), idx_z(ind_nan)) = ...
        nanmean(fooof_exponents_rest(idx_x(ind_nan), :, idx_z(ind_nan)),2);
end
clear idx_xyz idx_x idx_y idx_z

save(fullfile(pn.data, 'foof_exponents.mat'), 'fooof_exponents_rest', 'fooof_exponents')

%% export for further processing

IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

IDs{2} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

summaryIdx = ismember(STSWD_summary.IDs, IDs{1});
SlopeFits.IDs = IDs{1};
SlopeFits.linFit_fooof = fooof_exponents(summaryIdx,:,:);

save(fullfile(pn.data, 'C_SlopeFits_fooof_YA.mat'), 'SlopeFits')

summaryIdx = ismember(STSWD_summary.IDs, IDs{2});
SlopeFits.IDs = IDs{2};
SlopeFits.linFit_fooof = fooof_exponents(summaryIdx,:,:);

save(fullfile(pn.data, 'C_SlopeFits_fooof_OA.mat'), 'SlopeFits')
