#!/bin/bash

fun_name="a_calculateSpectra"
job_name="stsw_1f"

rootpath="$(pwd)/.."
rootpath=$(builtin cd $rootpath; pwd)

mkdir ${rootpath}/log

# path to the text file with all subject ids:
path_ids="${rootpath}/code/id_list.txt"
# read subject ids from the list of the text file
IDS=$(cat ${path_ids} | tr '\n' ' ')

for subj in $IDS; do 
  	sbatch \
  		--job-name ${job_name}_${subj} \
  		--cpus-per-task 2 \
  		--mem 4gb \
  		--time 01:00:00 \
  		--output ${rootpath}/log/${job_name}_${subj}.out \
  		--workdir . \
  		--wrap=". /etc/profile ; module load matlab/R2016b; matlab -nodisplay -r \"${fun_name}('${rootpath}','${subj}')\""
done