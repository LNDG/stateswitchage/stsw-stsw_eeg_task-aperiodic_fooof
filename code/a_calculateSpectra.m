function a_calculateSpectra(rootpath, id)

% Calculate 1/f spectra for StateSwitch Dynamic

if ismac % run if function is not pre-compiled
    id = '1126'; % test for example subject
    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..'))
    rootpath = pwd;
end

pn.dataIn       = fullfile(rootpath, '..', 'X1_preprocEEGData');
pn.dataOut      = fullfile(rootpath, 'data', 'B_1_f_fooof');
    if ~exist(pn.dataOut); mkdir(pn.dataOut); end
pn.tools        = fullfile(rootpath, '..', 'aperiodic', 'tools');
    addpath(pn.tools);
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;

%% load data

display(['processing ID ' id]);

load(fullfile(pn.dataIn, [id, '_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat']), 'data');

 %% CSD transform

TrlInfo = data.TrlInfo;
TrlInfoLabels = data.TrlInfoLabels;

csd_cfg = [];
csd_cfg.elecfile = 'standard_1005.elc';
csd_cfg.method = 'spline';
data = ft_scalpcurrentdensity(csd_cfg, data);
data.TrlInfo = TrlInfo; clear TrlInfo;
data.TrlInfoLabels = TrlInfoLabels; clear TrlInfoLabels;

%% calculate FFT

for indCond = 1:4
    cfg = [];
    cfg.trials = find(data.TrlInfo(:,8)==indCond);
    curData = ft_redefinetrial(cfg, data);

    curData = rmfield(curData, 'TrlInfo');
    curData = rmfield(curData, 'TrlInfoLabels');

    param.FFT.trial       = 'all';
    param.FFT.method      = 'mtmconvol';       % analyze entire spectrum; multitaper
    param.FFT.output      = 'pow';          % only get power values
    param.FFT.keeptrials  = 'no';           % return individual trials
    param.FFT.taper       = 'hanning';      % use hanning windows
    param.FFT.tapsmofrq   = 2;
    param.FFT.foi         = 1:.5:80;
    param.FFT.t_ftimwin   = repmat(2.5,1,numel(param.FFT.foi));
    param.FFT.toi         = 4.75; % 3.5 - 6 (500ms to stim offset)
    param.FFT.pad         = 20;

    tmpFreq = ft_freqanalysis(param.FFT, curData);

%         figure; plot(log10(param.FFT.foi), log10(squeeze(nanmean(tmpFreq.powspctrm(55:60,:),1))))

    if indCond == 1
        freq = tmpFreq;
        freq.powspctrm = [];
        freq.powspctrm(1,:,:) = tmpFreq.powspctrm;
    else
        freq.powspctrm(indCond,:,:) = tmpFreq.powspctrm;
    end
    clear tmpFreq;

end

%% save output

save(fullfile(pn.dataOut, [id, '_1f_fooof.mat']), 'freq');