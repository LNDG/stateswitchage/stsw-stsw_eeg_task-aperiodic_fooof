clear all; cla; clc; restoredefaultpath;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data');
pn.data_mse = fullfile(rootpath, '..', 'mse', 'data');

%load(fullfile(pn.data, 'm01_LV1.mat'), 'LV1')

load('/Users/kosciessa/Desktop/tardis/LNDG/stateswitch/stsw_eeg_task/aperiodic_fooof/data/g01_LV1.mat')
%load('/Users/kosciessa/Desktop/tardis/LNDG/stateswitch/stsw_eeg_task/mse/data/h01_LV1.mat')

pn.data_tfr    = fullfile(rootpath, '..', 'tfr', 'data');
pn.data_fooof    = fullfile(rootpath, '..', 'aperiodic_fooof', 'data');
pn.summary	= fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');
pn.tools        = fullfile(rootpath, '..', 'mse', 'tools');
    addpath(fullfile(pn.tools, 'BrewerMap'));
    addpath(fullfile(pn.tools, 'shadedErrorBar'));

%% load data

IDs = LV1.IDs;

ageIdx{1} = find(cellfun(@str2num, IDs, 'un', 1)<2000);
ageIdx{2} = find(cellfun(@str2num, IDs, 'un', 1)>2000);

MergedPow = [];
for id = 1:length(IDs)
    load(fullfile(pn.data_fooof, 'B_1_f_fooof', [IDs{id}, '_1f_fooof.mat']), 'freq');
    MergedPow(id,:,:,:) = freq.powspctrm;
end

mergedData = squeeze(nanmean(MergedPow(:,:,51:60,:),3));

colGrey = brewermap(4,'Greys');
colBlue = brewermap(4,'Reds');

% %% plot occipital spectra YA
% 
% idx_chan = 58:60;
% idx_ids = ageIdx{1};
% 
% cBrew = brewermap(4,'RdBu');
% cBrew = flipud(cBrew);
% h = figure('units','centimeter','position',[0 0 35 9]);
% subplot(1,3,1); hold on;
%     condAvg = squeeze(nanmean(nanmean(log10(MergedPow(idx_ids,:,idx_chan,:)),3),2));
%     curData = squeeze(nanmean(log10(MergedPow(idx_ids,1,idx_chan,:)),3));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
%     curData = squeeze(nanmean(log10(MergedPow(idx_ids,4,idx_chan,:)),3));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
%     xlim([0 1.9]); %ylim([-10 -6.5])
%     xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');


%% md split on LV1: YA

[~, sortInd_LV1] = sort(LV1.linear(ageIdx{1}),'ascend');
sortInd_LV1_bottom = sortInd_LV1(1:ceil(numel(sortInd_LV1)/2));
sortInd_LV1_top = sortInd_LV1(ceil(numel(sortInd_LV1)/2)+1:end);

h = figure('units','normalized','position',[.1 .1 .3 .2]);
subplot(1,2,1);
    cla; hold on;
    curIDs = ageIdx{1}(sortInd_LV1_top);
    grandAverage = squeeze(nanmean(log10(mergedData(curIDs,1:4,:,:)),2));
    curData = squeeze(nanmean(log10(mergedData(curIDs,1,:,:)),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(log10(mergedData(curIDs,2,:,:)),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, 'lineprops', {'color', colBlue(2,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(log10(mergedData(curIDs,3,:,:)),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, 'lineprops', {'color', colBlue(3,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(log10(mergedData(curIDs,4,:,:)),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    
    legend([l1.mainLine, l4.mainLine], {'Load 1'; 'Load 4'}, 'location', 'NorthWest'); legend('boxoff')
    xlabel('[] (ms); response-locked')
    ylabel({'1f';'(a.u.; normalized)'});
    xlabel({'[] (s)'});
    title('LV1 top');
    
subplot(1,2,2);
    cla; hold on;
    curIDs = ageIdx{1}(sortInd_LV1_bottom);
    grandAverage = squeeze(nanmean(log10(mergedData(curIDs,1:4,:,:)),2));
    curData = squeeze(nanmean(log10(mergedData(curIDs,1,:,:)),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(log10(mergedData(curIDs,2,:,:)),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, 'lineprops', {'color', colBlue(2,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(log10(mergedData(curIDs,3,:,:)),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, 'lineprops', {'color', colBlue(3,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(log10(mergedData(curIDs,4,:,:)),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    
    legend([l1.mainLine, l4.mainLine], {'Load 1'; 'Load 4'}, 'location', 'NorthWest'); legend('boxoff')
    xlabel('[] (ms); response-locked')
    ylabel({'1f';'(a.u.; normalized)'});
    title('LV1 bottom');
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% md split on LV1: OA

[~, sortInd_LV1] = sort(LV1.linear(ageIdx{2}),'ascend');
sortInd_LV1_bottom = sortInd_LV1(1:ceil(numel(sortInd_LV1)/2));
sortInd_LV1_top = sortInd_LV1(ceil(numel(sortInd_LV1)/2)+1:end);

h = figure('units','normalized','position',[.1 .1 .3 .2]);
subplot(1,2,1);
    cla; hold on;
    curIDs = ageIdx{2}(sortInd_LV1_top);
    grandAverage = squeeze(nanmean(log10(mergedData(curIDs,1:4,:,:)),2));
    curData = squeeze(nanmean(log10(mergedData(curIDs,1,:,:)),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(log10(mergedData(curIDs,2,:,:)),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, 'lineprops', {'color', colBlue(2,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(log10(mergedData(curIDs,3,:,:)),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, 'lineprops', {'color', colBlue(3,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(log10(mergedData(curIDs,4,:,:)),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    
    legend([l1.mainLine, l4.mainLine], {'Load 1'; 'Load 4'}, 'location', 'NorthWest'); legend('boxoff')
    xlabel('[] (ms); response-locked')
    ylabel({'1f';'(a.u.; normalized)'});
    xlabel({'[] (s)'});
    title('LV1 top');
    
subplot(1,2,2);
    cla; hold on;
    curIDs = ageIdx{2}(sortInd_LV1_bottom);
    grandAverage = squeeze(nanmean(log10(mergedData(curIDs,1:4,:,:)),2));
    curData = squeeze(nanmean(log10(mergedData(curIDs,1,:,:)),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(log10(mergedData(curIDs,2,:,:)),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, 'lineprops', {'color', colBlue(2,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(log10(mergedData(curIDs,3,:,:)),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, 'lineprops', {'color', colBlue(3,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(log10(mergedData(curIDs,4,:,:)),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    
    legend([l1.mainLine, l4.mainLine], {'Load 1'; 'Load 4'}, 'location', 'NorthWest'); legend('boxoff')
    xlabel('[] (ms); response-locked')
    ylabel({'1f';'(a.u.; normalized)'});
    title('LV1 bottom');
set(findall(gcf,'-property','FontSize'),'FontSize',18)
