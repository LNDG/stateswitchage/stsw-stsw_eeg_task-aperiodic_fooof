%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SETTINGS FOR "FT_REJECTVISUAL" VIEWER: %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This settings file will be read by the TrialCleanerGUI.
% - Settings here will be applied to "ft_rejectvisual".
%
% Usage:
%  a)  Settings under "DO NOT EDIT" are handled by [MEG]PLS. Do not alter these.
%  b)  Change other values as needed. See "help" command for FieldTrip section.
%  c)  Optional settings can be uncommented and specified.
%
function cfgReject = Settings_RejectVisual



%--- "FT_REJECTVISUAL" SETTINGS: ---%
%-----------------------------------%

% DO NOT EDIT:
cfgReject.method = [];  % Selected in GUI


% GENERAL SETTINGS:
cfgReject.channel     = 'MEG';
cfgReject.trials      = 'all';
cfgReject.keepchannel = 'no';   % Recommended: Leave on 'no'
cfgReject.alim        = 1e-12;


% OPTIONAL SETTINGS:
%  cfgReject.latency
%  cfgReject.metric

%  cfgReject.eogscale
%  cfgReject.ecgscale
%  cfgReject.emgscale
%  cfgReject.megscale
%  cfgReject.gradscale
%  cfgReject.magscale


% OPTIONAL PREPROCESSING OPTIONS FOR VIEWING CAN BE SPECIFIED AS FOLLOWS:
% Note: The preprocessing is NOT applied to the output data.
% See "help ft_preprocessing" for available options.
%  cfgReject.preproc.bpfilter
%  cfgReject.preproc.bpfreq
