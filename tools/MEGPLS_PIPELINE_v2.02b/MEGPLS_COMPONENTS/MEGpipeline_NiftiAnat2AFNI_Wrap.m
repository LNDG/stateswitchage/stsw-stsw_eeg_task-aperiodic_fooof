%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Converts anatomical NIFTI files to AFNI:     %
% Inputs: Normalised MRI NIFTI files.          %
% Last modified: Jan. 15, 2014                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_NiftiAnat2AFNI_Wrap(BuilderMat, TargetNiftiType)


% Make sure java paths added:
UseProgBar = CheckParforJavaPaths;


% Load builder .mat file:
Builder      = load(BuilderMat);
name         = Builder.name;
paths        = Builder.paths;
time         = Builder.time;
PipeSettings = Builder.PipeSettings;


% Clear existing ErrorLog & Diary:
switch TargetNiftiType
    case 'PreprocMRINii'
        RunSection = 'MakePreprocMRIAfni';
        
    case 'NormMRINii'
        RunSection = 'MakeNormMRIAfni';     
end

if exist(['ErrorLog_',RunSection,'.txt'], 'file')
    system(['rm ErrorLog_',RunSection,'.txt']);
end
if exist(['Diary_',RunSection,'.txt'], 'file')
    system(['rm Diary_',RunSection,'.txt']);
end

diary(['Diary_',RunSection,'.txt']);
ErrLog = fopen(['ErrorLog_',RunSection,'.txt'], 'a');



%============================================%
% CONVERT ANATOMICAL NIFTI INTO AFNI-FORMAT: %
%============================================%

for g = 1:length(name.GroupID)
    NumSubj = length(name.SubjID{g});
    
    if UseProgBar == 1
        ppm = ParforProgMon...
            (['CONVERTING MRI FILES TO AFNI:  ',name.GroupID{g},'.  '], NumSubj, 1, 700, 80);
    end
    
    % Note: Do NOT parfor this due to 3dWarp command.
    for s = 1:length(name.SubjID{g})
        
        
        %--- Make AFNI file for PreprocMRI: ---%
        %--------------------------------------%
        
        if strcmp(TargetNiftiType, 'PreprocMRINii')
            
            % Convert to AFNI:
            if ~exist(paths.MRIdataNii{g}{s}, 'file')
                fprintf(ErrLog, ['ERROR: Input NIFTI file missing:'...
                    '\n %s \n\n'], paths.MRIdataNii{g}{s});
                continue;
            end
            
            CheckSavePath(paths.MRIdataAfni{g}{s}, RunSection);
            
            MEGpipeline_Nifti2Afni_Guts...
                (PipeSettings, paths.MRIdataNii{g}{s}, paths.MRIdataAfni{g}{s})
            
            
            % Deoblique non-normalised MRIs for viewing:
            % Note: Deoblique command outputs file "warped+tlrc.BRIK/HEAD" in current dir.
            if exist('warped+tlrc.BRIK', 'file') || exist('warped+tlrc.HEAD', 'file')
                delete('warped+tlrc.BRIK');
                delete('warped+tlrc.HEAD');
                
                if exist('warped+tlrc.BRIK', 'file')
                    disp('ERROR: Could not remove remnant "warped+tlrc" files.');
                    fprintf(ErrLog, ['ERROR: Failed to deoblique non-normalised MRI file:'...
                        '\n %s \n\n'], paths.MRIdataAfni{g}{s});
                    continue;
                end
            end
            
            system(['3dWarp -deoblique ',paths.MRIdataAfni{g}{s}]);
            
            
            % Remove and replace oblique files with deobliqued ones:
            [TargetFolder, TargetFile, ~] = fileparts(paths.MRIdataAfni{g}{s});
            delete([TargetFolder,'/',TargetFile,'.BRIK']);
            delete([TargetFolder,'/',TargetFile,'.HEAD']);
            
            if exist(paths.MRIdataAfni{g}{s}, 'file')
                disp('ERROR: Could not replace oblique MRI files with deobliqued ones.');
                fprintf(ErrLog, ['ERROR: Failed to deoblique non-normalised MRI file:'...
                    '\n %s \n\n'], paths.MRIdataAfni{g}{s});
                continue;
            end
            
            system(['3dcopy warped+tlrc.BRIK ',paths.MRIdataAfni{g}{s}]);
            delete('warped+tlrc.BRIK');
            delete('warped+tlrc.HEAD');
            
            
            % Resample if needed:
            ResampledBrik = [];
            ResampledBrik = MEGpipeline_AfniResampleAnat...
                (paths.MRIdataAfni{g}{s}, paths.Afni4DSource{g}{s,1});
            
            ResampledFilePaths{g}{s} = ResampledBrik;
            
        end
        
        
        %--- Make AFNI file for NormMRI: ---%
        %-----------------------------------%
        
        if strcmp(TargetNiftiType, 'NormMRINii')
            
            % Convert to AFNI:
            if ~exist(paths.NormMRINii{g}{s}, 'file')
                fprintf(ErrLog, ['ERROR: Input NIFTI file missing:'...
                    '\n %s \n\n'], paths.NormMRINii{g}{s});
                continue;
            end
            
            CheckSavePath(paths.NormMRIAfni{g}{s}, RunSection);
            
            MEGpipeline_Nifti2Afni_Guts...
                (PipeSettings, paths.NormMRINii{g}{s}, paths.NormMRIAfni{g}{s})
            
            % Resample if needed:
            ResampledBrik = [];
            ResampledBrik = MEGpipeline_AfniResampleAnat...
                (paths.NormMRIAfni{g}{s}, paths.Afni4DNormSource{g}{s,1});
            
            ResampledFilePaths{g}{s} = ResampledBrik;

        end
        
        
        if UseProgBar == 1 && mod(s, 1) == 0
            ppm.increment();  % move up progress bar
        end
        
    end  % Subj
    if UseProgBar == 1
        ppm.delete();
    end
    
end  % Group



%==========================%
% CHECKS FOR OUTPUT FILES: %
%==========================%

for g = 1:length(name.GroupID)
    for s = 1:length(name.SubjID{g})
        
        if strcmp(TargetNiftiType, 'PreprocMRINii')
            if ~exist(paths.MRIdataAfni{g}{s}, 'file')
                fprintf(ErrLog, ['ERROR: Output AFNI anatomy file missing:'...
                    '\n %s \n\n'], paths.MRIdataAfni{g}{s});
            end
            if ~exist(ResampledFilePaths{g}{s}, 'file') && ...
                    ~strcmp(ResampledFilePaths{g}{s}, paths.MRIdataAfni{g}{s})
                fprintf(ErrLog, ['ERROR: Resampled AFNI anatomy file missing:'...
                    '\n %s \n\n'], ResampledFilePaths{g}{s});
            end
            
        elseif strcmp(TargetNiftiType, 'NormMRINii')
            if ~exist(paths.NormMRIAfni{g}{s}, 'file')
                fprintf(ErrLog, ['ERROR: Output AFNI anatomy file missing:'...
                    '\n %s \n\n'], paths.NormMRIAfni{g}{s});
            end
            if ~exist(ResampledFilePaths{g}{s}, 'file') && ...
                    ~strcmp(ResampledFilePaths{g}{s}, paths.NormMRIAfni{g}{s})
                fprintf(ErrLog, ['ERROR: Resampled AFNI anatomy file missing:'...
                    '\n %s \n\n'], ResampledFilePaths{g}{s});
            end
        end
        
    end  % Subj
end  %Group

        

%=================%

if exist([pwd,'/ErrorLog_',RunSection,'.txt'], 'file')
    LogCheck = dir(['ErrorLog_',RunSection,'.txt']);
    if LogCheck.bytes ~= 0  % File not empty
        open(['ErrorLog_',RunSection,'.txt']);
    else
        delete(['ErrorLog_',RunSection,'.txt']);
    end
end

if exist([pwd,'/ErrorLog_Nifti2Afni.txt'], 'file')
    LogCheck = dir('ErrorLog_Nifti2Afni.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_Nifti2Afni.txt');
    else
        delete('ErrorLog_Nifti2Afni.txt');
    end
end

fclose(ErrLog);
diary off
