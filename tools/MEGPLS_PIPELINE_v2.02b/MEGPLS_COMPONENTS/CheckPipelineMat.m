%=============================================================%
% Checks .mat files for [MEG]PLS without having to load them. %
% Similar to LoadFTmat except the file is NOT loaded.         %                                                            %
% If any errors are detected, they will be added to ErrLog.   %
%                                                             %
% Note: This is for cases where "cfg.inputfile" is used.      %
% Allows files to be checked before being specified in cfg.   %
% Last modified: Sept. 7, 2014                                %
%=============================================================%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function Success = CheckPipelineMat(PipelineMat, RunSection)
LoadErrLog = fopen(['ErrorLog_',RunSection,'.txt'], 'a');


% Check if file exists:
if ~exist(PipelineMat, 'file')
	fprintf(LoadErrLog, 'ERROR: Input file does not exist: \n %s \n\n', PipelineMat);
    disp('ERROR: Input .mat file is missing.')
    
	Success = 0;
    return;
end


% Make sure file is .mat:
[~, ~, FileExt] = fileparts(PipelineMat);

if ~strcmp(FileExt, '.mat')
    fprintf(LoadErrLog, 'ERROR: Input file is not a .mat: \n %s \n\n', PipelineMat);
    disp('ERROR: Failed to load. File is not a .mat.')
    
    Success = 0;
    return;
end


% Check if .mat is FT compatible:
CheckVars = whos('-file', PipelineMat);

if isempty(CheckVars)
    fprintf(LoadErrLog, 'ERROR: Input .mat file is empty: \n %s \n\n', PipelineMat);
    disp('ERROR: Input .mat file is empty.')
    
	Success = 0;
    return;
end

if length(CheckVars) > 1
    fprintf(LoadErrLog, ['ERROR: Loaded .mat is not FieldTrip compatible.'...
		'\nThe .mat file should only contain a single variable:'...
		'\n %s \n\n'], PipelineMat);
    disp('ERROR: Input .mat file is not FieldTrip compatible.')
    
	Success = 0;
    return;
end

Success = 1;
fclose(LoadErrLog);
