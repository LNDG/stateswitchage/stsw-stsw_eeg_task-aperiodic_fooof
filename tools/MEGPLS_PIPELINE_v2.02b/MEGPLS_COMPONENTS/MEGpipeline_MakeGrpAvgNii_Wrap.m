%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Creates 3D and 4D NIFTI files for GRPAVG NormSource files. %
% Note: NormSource files are already interpolated.           %
% Last updated: Jan. 15, 2014                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_MakeGrpAvgNii_Wrap(BuilderMat)


% Make sure java paths added:
UseProgBar = CheckParforJavaPaths;


% Load builder .mat file:
Builder      = load(BuilderMat);
name         = Builder.name;
paths        = Builder.paths;
time         = Builder.time;
FTcfg        = Builder.FTcfg;
PipeSettings = Builder.PipeSettings;


% Clear existing Errorlog & Diary:
if exist('ErrorLog_MakeGrpAvgNii.txt', 'file')
    delete('ErrorLog_MakeGrpAvgNii.txt');
end
if exist('Diary_MakeGrpAvgNii.txt', 'file')
    delete('Diary_MakeGrpAvgNii.txt');
end

diary Diary_MakeGrpAvgNii.txt
ErrLog = fopen('ErrorLog_MakeGrpAvgNii.txt', 'a');



%===================================================%
% GENERATE NIFTI FILES FOR GROUP-AVG NORMSRC FILES: %
%===================================================%

AvgParam = FTcfg.WriteNormSrc.parameter;
if strcmp(AvgParam(1:4), 'avg.')
    AvgParam(1:4) = [];
end
            
            
for g = 1:length(name.GroupID)
    NumCond = length(name.CondID);
    
    if UseProgBar == 1
        ppm = ParforProgMon...
            (['GENERATING GROUP-AVG NIFTI FILES:  ',name.GroupID{g},'.  '], NumCond, 1, 700, 80);
    end
    
    for c = 1:length(name.CondID)
        
        
        %--- Create 3D NIFTI files: ---%
        %------------------------------%
        
        parfor t = 1:size(time.Windows, 1)
            CheckSavePath(paths.GrpAvgNii{g}{c,t}, 'MakeGrpAvgNii');  % Ensure no old remnants
            
            cfgVolumeWrite           = [];
            cfgVolumeWrite           = FTcfg.WriteNormSrc;  % Same settings as WriteNormSrc
            cfgVolumeWrite.parameter = AvgParam;
            
            MEGpipeline_MakeNifti3D_Guts...
                (cfgVolumeWrite, paths.GrpAvg{g}{c,t}, paths.GrpAvgNii{g}{c,t})
            
        end  % Time
        
        
        %--- Merge 3D NIFTI files into 4D NIFTI: ---%
        %-------------------------------------------%
        
        % Make list of files to merge:
        MergeFileList = [];
        MissingFiles  = 0;
        
        for t = 1:size(time.Windows, 1)  % DO NOT PARFOR HERE, WANT PROPER ORDER
            MergeFileList{t}  = paths.GrpAvgNii{g}{c,t};  %** Is the ",1" needed?
            
            if ~exist(paths.GrpAvgNii{g}{c,t}, 'file')
                [Folder, File, ~] = fileparts(paths.GrpAvgNii{g}{c,t});
                
                if MissingFiles == 0
                    MissingFiles = 1;
                    fprintf(ErrLog, ['\nERROR: GrpAvg 3D .nii file(s) missing:'...
                        '\n %s \n'], Folder);
                end
                
                fprintf(ErrLog, ' - Time: %s \n', [File,'.nii']);
            end
        end
        
        if MissingFiles == 1
            fprintf(ErrLog, ['ERROR: Failed to generate 4D-NIFTI file (missing 3D-NIFTI files):'...
                '\n %s \n\n'], paths.Nifti4DGrpAvg{g}{c});
            
            disp('ERROR: Missing 3D .nii file(s). Skipping dataset.')
            continue;
        end
        
        % Merge 3D NIFTI files into 4D NIFTI:
        CheckSavePath(paths.Nifti4DGrpAvg{g}{c}, 'MakeGrpAvgNii');
        MEGpipeline_MakeNifti4D_Guts(MergeFileList, paths.Nifti4DGrpAvg{g}{c})
        
        % Remove intermediate 3D NIFTI files:
        if strcmp(PipeSettings.Afni4D.KeepNifti3D, 'no')
            for f = 1:length(MergeFileList)
                delete([MergeFileList{f}]);
            end
        end
        
        
        if UseProgBar == 1 && mod(c, 1) == 0
            ppm.increment();  % move up progress bar
        end
        
    end  % Cond
    if UseProgBar == 1
        ppm.delete();
    end
    
end  % Group

if strcmp(PipeSettings.Afni4D.KeepNifti3D, 'no')
    if exist([paths.AnalysisID,'/NORM_SOURCE_Nifti3D'], 'dir')
        system(['rm -R ',paths.AnalysisID,'/NORM_SOURCE_Nifti3D']);
    else
        disp('WARNING: Could not find NORM_SOURCE_Nifti3D folder for removal.');
    end
end



%==========================================================%
% GENERATES TIME-LEGEND FOR INDICES IN NIFTI & AFNI FILES: %
%==========================================================%

if exist([paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt'], 'file')
	delete([paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt']);
end
TimeLegend = fopen('NiftiAfni4D_TimeLegend.txt', 'a');

fprintf(TimeLegend, ['/// IMPORTANT: ///'...
	'\n - In the AFNI-VIEWER, voxel and time indices start at 0.    '...
    '\n   In the data however, voxel and time indices start at 1. \n'...
    '\n - Therefore, when selecting indices for use outside of the  '...
    '\n   AFNI-viewer or AFNI functions, make sure you are using the'...
    '\n   data index, and NOT the AFNI-viewer index. \n\n']);

fprintf(TimeLegend, ['/// TIME LEGEND: ///'...
	'\n AFNI-Viewer ------- AFNI/NIFTI ------- TIME-Interval:'...
	'\n ViewerIndex ------- Data Index ------- In seconds:']);

for t = 1:size(time.Windows, 1)
	fprintf(TimeLegend, '\n---- %s --------------- %s ------------ %s',...
		num2str(t-1), num2str(t), time.str.Windows{t,3});
end

fclose(TimeLegend);
if ~isequal(pwd, paths.AnalysisID)
    movefile('NiftiAfni4D_TimeLegend.txt', [paths.AnalysisID,'/'], 'f');
end



%==========================%
% CHECKS FOR OUTPUT FILES: %
%==========================%

for g = 1:length(name.GroupID)
    for c = 1:length(name.CondID)
        
        if ~exist(paths.Nifti4DGrpAvg{g}{c}, 'file')
            fprintf(ErrLog, ['ERROR: Output 4D-NIFTI file missing:'...
                '\n %s \n\n'], paths.Nifti4DGrpAvg{g}{c});
        end
        
    end  % Cond
end  % Group

if ~exist([paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt'], 'file')
    fprintf(ErrLog, ['ERROR: Time-legend file for NIFTI/AFNI files missing:'...
        '\n %s \n\n'], [paths.AnalysisID,'/NiftiAfni4D_TimeLegend.txt']);
end



%=================%

if exist([pwd,'/ErrorLog_MakeGrpAvgNii.txt'], 'file')
    LogCheck = dir('ErrorLog_MakeGrpAvgNii.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_MakeGrpAvgNii.txt');
    else
        delete('ErrorLog_MakeGrpAvgNii.txt');
    end
end

if exist([pwd,'/ErrorLog_MakeNifti3D.txt'], 'file')
    LogCheck = dir('ErrorLog_MakeNifti3D.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_MakeNifti3D.txt');
    else
        delete('ErrorLog_MakeNifti3D.txt');
    end
end

if exist([pwd,'/ErrorLog_MakeNifti4D.txt'], 'file')
    LogCheck = dir('ErrorLog_MakeNifti4D.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_MakeNifti4D.txt');
    else
        delete('ErrorLog_MakeNifti4D.txt');
    end
end

fclose(ErrLog);
diary off
