%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Checks dims & orientation of AFNI anatomy file.                    %
% Resamples anatomy to dimensions & orientation of functional image. %
% Last modified: Feb. 25, 2014                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Usage:
%  OutputBrik = MEGpipeline_AfniResampleAnat(AnatBrik, FuncBrik)
%
% Inputs:
%  AnatBrik = AFNI anatomy file to check (and resample to functional if needed).
%  FuncBrik = AFNI functional file that anatomy will be compared to.
%
% Output:
%  OutputBrik = If resample was needed, path of resampled AFNI anatomy is output.
%               If resample was not needed, path of original AnatBrik is output.


% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function OutputBrik = MEGpipeline_AfniResampleAnat(AnatBrik, FuncBrik)

if ~exist(AnatBrik, 'file')
    disp('Error: Specified AFNI anatomy file does not exist.');
    OutputBrik = [];
    return;
end
if ~exist(FuncBrik, 'file')
    disp('Error: Specified AFNI functional file does not exist.');
    OutputBrik = [];
    return;
end


% Assemble paths and remove existing files:
[FuncFolder, FuncFile, ~] = fileparts(FuncBrik);
[AnatFolder, AnatFile, ~] = fileparts(AnatBrik);

AfniViewLabel             = FuncFile(end-4:end);
AnatFile(end-4:end)       = [];  % Remove +view to get just filename

ResampledName = [AnatFolder,'/',AnatFile,'_resampled'];
ResampledBrik = [AnatFolder,'/',AnatFile,'_resampled',AfniViewLabel,'.BRIK'];

if exist(ResampledBrik, 'file')
    delete([ResampledName,AfniViewLabel,'.BRIK']);
    delete([ResampledName,AfniViewLabel,'.HEAD']);
end


% Load BRIK files:
CurrentDir = pwd;
Opt.format = 'vector';

cd(AnatFolder);  % Work-around for occasional bug
[~, Anat, AnatInfo, ~] = BrikLoad(AnatBrik, Opt);

cd(FuncFolder);
[~, Func, FuncInfo, ~] = BrikLoad(FuncBrik, Opt);
cd(CurrentDir);


% Compare AnatBrik to FuncBrik to see if resample needed:
AnatNumDims = length(size(Anat));
FuncNumDims = length(size(Func));

if AnatNumDims < FuncNumDims
    CheckFuncDim = size(Func);
    CheckFuncDim = CheckFuncDim(1:AnatNumDims);
    
elseif AnatNumDims > FuncNumDims
    CheckFuncDim = size(Anat);
    CheckFuncDim = CheckFuncDim(1:FuncNumDims);
    
else
    CheckFuncDim = size(Func);
end

AnatOrient = AnatInfo.ORIENT_SPECIFIC;
FuncOrient = FuncInfo.ORIENT_SPECIFIC;

if ~isequal(size(Anat), CheckFuncDim) || ~isequal(AnatOrient, FuncOrient)
    system(['3dresample -master ',FuncBrik,' -inset ',AnatBrik,' -prefix ',ResampledName]);
    OutputBrik = ResampledBrik;
else
    OutputBrik = AnatBrik;
end