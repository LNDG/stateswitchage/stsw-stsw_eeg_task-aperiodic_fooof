%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computes covariance / CSD and source-filter via Fieldtrip. %
% Last modified: Jan. 15, 2014                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Usage:
%  MEGpipeline_SourceFilter_Guts...
%    (PipeSettings, FTcfg, Hdm, Lead, MEGdata, OutpathCovCsd, OutpathFilter)
%
% Inputs:
%  Note: PipeSettings & FTcfg can be generated & imported from a Builder .mat file.
%  PipeSettings.Freq.CsdWindow = 'all' or [TimeStart, TimeEnd] (for freq-domain methods).
%                              = [];                           (for time-domain methods).
%
%  FTcfg.Timelock = Fieldtrip config for ft_timelockanalysis (for time-domain methods).
%  FTcfg.Freq     = Fieldtrip config for ft_freqanalysis     (for freq-domain methods).
%  FTcfg.Source   = Fieldtrip config for ft_sourceanalysis.
%
%  Hdm     = Path to FT .mat headmodel file OR loaded headmodel structure.
%  Lead    = Path to FT .mat leadfield file OR loaded leadfield structure.
%  MEGdata = Path to FT .mat MEGdata file   OR loaded MEGdata structure.
%
%  OutpathCovCsd = Output /path/filename for COV or CSD file.
%  OutpathFilter = Output /path/filename for source filter.


% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_SourceFilter_Guts...
    (PipeSettings, MainFTcfg, Hdm, Lead, MEGdata, OutpathCovCsd, OutpathFilter)


% Check number of input & output arguments:
if nargin ~= 7
    error('ERROR: Incorrect number of arguments. See help for usage info.')
end


% If input files are .mat paths (and NOT loaded structures):
if ~isstruct(MEGdata)
    CheckInput = CheckPipelineMat(MEGdata, 'SourceFilter');
    if CheckInput == 0
        return;
    end
end
if ~isstruct(Hdm)
    CheckInput = CheckPipelineMat(Hdm, 'SourceFilter');
    if CheckInput == 0
        return;
    end
end
if ~isstruct(Lead)
    CheckInput = CheckPipelineMat(Lead, 'SourceFilter');
    if CheckInput == 0
        return;
    end
end
    

% Check source method:
if ismember(MainFTcfg.Source.method, {'dics', 'pcc'})
    SourceMethodDomain = 'Freq';  % DICS & PCC are for freq-domain data.
else
    SourceMethodDomain = 'Time';  % All other methods are for time-domain data.
end


%--- Compute Covariance or CSD matrix: ---%
%-----------------------------------------%

% Compute covariance matrix for time-domain source methods:
if strcmp(SourceMethodDomain, 'Time')
    disp('Computing covariance matrix:')
    
    CheckSavePerms(OutpathCovCsd, 'SourceFilter');
    
    FTcfg.Timelock                   = [];
    FTcfg.Timelock                   = MainFTcfg.Timelock;
	FTcfg.Timelock.covariance        = 'yes';
    FTcfg.Timelock.keeptrials        = 'no';   % Disable for COV & filter calculation.
    FTcfg.Timelock.outputfile        = OutpathCovCsd;
    FTcfg.Timelock.outputfilepresent = 'overwrite';
    
    if ~isstruct(MEGdata)  % If input is .mat path
        FTcfg.Timelock.inputfile = MEGdata;
        ft_timelockanalysis(FTcfg.Timelock)
    else                   % If input is loaded structure
        ft_timelockanalysis(FTcfg.Timelock, MEGdata)
        MEGdata = [];      % Free memory
    end
    
    
% Compute CSD matrix for frequency-domain source methods (DICS & PCC):
elseif strcmp(SourceMethodDomain, 'Freq')
    disp('Computing CSD matrix:')
    
    if strcmp(PipeSettings.Freq.CsdWindow, 'all')
        if ~isstruct(MEGdata)
            ResizedData = LoadFTmat(MEGdata, 'SourceFilter');
        else
            ResizedData = MEGdata;
            MEGdata     = [];  % Free memory
        end
        
    else
        cfgCsdWindow        = [];
        cfgCsdWindow.toilim = PipeSettings.Freq.CsdWindow;
        
        if ~isstruct(MEGdata)
            cfgCsdWindow.inputfile = MEGdata;
            ResizedData = ft_redefinetrial(cfgCsdWindow)
        else
            ResizedData = ft_redefinetrial(cfgCsdWindow, MEGdata)
            MEGdata     = [];  % Free memory
        end
    end
    
    CheckSavePerms(OutpathCovCsd, 'SourceFilter');
    
    FTcfg.Freq                   = [];
    FTcfg.Freq                   = MainFTcfg.Freq;
    FTcfg.Freq.output            = 'powandcsd';
    FTcfg.Freq.keeptrials        = 'no';   % Disable for CSD & filter calculation.
    FTcfg.Freq.outputfile        = OutpathCovCsd;
    FTcfg.Freq.outputfilepresent = 'overwrite';
    
    ft_freqanalysis(FTcfg.Freq, ResizedData)
    
end


%--- Compute source filter: ---%
%------------------------------%

CheckSavePerms(OutpathFilter, 'SourceFilter');

FTcfg.Source            = [];
FTcfg.Source            = MainFTcfg.Source;
FTcfg.Source.rawtrial   = 'no';  % Disable for COV/CSD & filter calculation
FTcfg.Source.keeptrials = 'no';  % Disable for COV/CSD & filter calculation

SourceMethod = FTcfg.Source.method;
FTcfg.Source.(SourceMethod).keepfilter = 'yes';  % Make sure keepfilter is on.

if ~isstruct(Hdm)
    FTcfg.Source.vol = LoadFTmat(Hdm, 'SourceFilter');
else
    FTcfg.Source.vol = Hdm;
end

if ~isstruct(Lead)
    FTcfg.Source.grid = LoadFTmat(Lead, 'SourceFilter');
else
    FTcfg.Source.grid = Lead;
end

CheckInput = CheckPipelineMat(OutpathCovCsd, 'SourceFilter');
if CheckInput == 1  % Success
    FTcfg.Source.inputfile = OutpathCovCsd;
else
    return;
end

disp('Computing source filter:')
SourceFilter = ft_sourceanalysis(FTcfg.Source)
FTcfg.Source = [];  % Free memory (hdm & lead)

% Remove "cfg.previous", and "cfg.callinfo" fields to save space!
if isfield(SourceFilter.cfg, 'previous')
    SourceFilter.cfg = rmfield(SourceFilter.cfg, 'previous');
end
if isfield(SourceFilter.cfg, 'callinfo')
    SourceFilter.cfg = rmfield(SourceFilter.cfg, 'callinfo');
end

% Save filter:
CheckSavePath(OutpathFilter, 'SourceFilter');
save(OutpathFilter, 'SourceFilter');
