%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Writes fieldtrip data to 3D NIFTI file. %
% Last modified: Jan. 15, 2014            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Usage:
%  MEGpipeline_MakeNifti3D_Guts(cfgVolumeWrite, InputData, Outpath)
%
% Inputs:
%  cfgVolumeWrite = FT config for ft_volumewrite.
%  InputData      = Loaded FieldTrip structure containing data OR path to FT .mat file.
%  Outpath        = Output /path/filename of 3D NIFTI file.


% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_MakeNifti3D_Guts(FTcfgVolumeWrite, InputData, OutpathNii)


% Check number of input arguments:
if nargin ~= 3
    error('ERROR: Incorrect number of input arguments. See help for usage info.')
end


% If InputData is path to FT .mat, load it:
if ~isstruct(InputData)
    InputData = LoadFTmat(InputData, 'MakeNifti3D');
    if isempty(InputData)
        return;
    end
end


% Set write parameters:
[FolderNii, NameNii, ~] = fileparts(OutpathNii);

TempName = NameNii;                     % Temp output name:
TempName(strfind(TempName, '.')) = [];  % ft_volumewrite cannot read dots.

cfgWrite          = [];
cfgWrite          = FTcfgVolumeWrite;
cfgWrite.filename = TempName;


% Reshape field if needed:
ParamName   = cfgWrite.parameter;
TargetParam = getsubfield(InputData, ParamName);

if ~isequal(size(TargetParam), InputData.dim)
    TargetParam = reshape(TargetParam, InputData.dim);
    InputData   = setsubfield(InputData, ParamName, TargetParam);
end


% Write 3D NIFTI and save:
CheckSavePath(OutpathNii, 'MakeNifti3D');
ft_volumewrite(cfgWrite, InputData)

Status = movefile([TempName,'.nii'], [FolderNii,'/',NameNii,'.nii'], 'f');

if Status == 0
    if exist([TempName,'.nii'], 'file')
        system(['rm ',TempName,'.nii']);  % If failed to move, delete remnant.
    end
    
    disp('ERROR: Failed to rename & move .nii file to output directory.')
    return;
end

