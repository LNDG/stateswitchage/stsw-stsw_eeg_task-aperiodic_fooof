[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## Aperiodic (1/f) slopes, fitted with fooof

The aperiodic 1/f slope of neural recordings is closely related to the sample entropy of broadband signals 17 and has been suggested as a proxy for ‘cortical excitability’ and excitation-inhibition balance 8. Spectral estimates were computed by means of a Fast Fourier Transform (FFT) over the final 2.5 s of the presentation period (to exclude onset transients) for linearly spaced frequencies between 1 and 80 Hz (Hanning-tapered segments zero-padded to 20 s, step-size of 0.5 Hz) and subsequently averaged. Power spectral density (PSD) slope estimates were derived via fooof (**REF**), which renders estimates specific to aperiodic variance. Note that slope estimates were inverted to produce negative slopes as they would be produced by regression. Load load effects on 1/f slopes were assessed via taskPLS, including estimates only from posterior-occipital channels.

## install fooof toolbox

To run fooof on tardis (*b_runFOOOF*), we need to install the fooof toolbox. Here, we create a virtual environment that contains the fooof toolbox, which will then be used for the respective jobs.

````
mkvirtualenv --python=$(which python3) fooof
pip install fooof
pip install matplotlib
workon fooof
````

**a_calculateSpectra**

- CSD transform
- TFR transform: final 2.5 s of stimulus presentation (skipping initial 500 ms)

````
    param.FFT.method      = 'mtmconvol';    % analyze entire spectrum; multitaper
    param.FFT.taper       = 'hanning';      % use hanning windows
    param.FFT.tapsmofrq   = 2;
    param.FFT.foi         = 1:.5:80;
    param.FFT.t_ftimwin   = repmat(2.5,1,numel(param.FFT.foi));
    param.FFT.toi         = 4.75; % 3.5 - 6 (500ms to stim offset)
    param.FFT.pad         = 20;
````

**b_runFOOOF**

- fit fooof model between 2 and 80 Hz

**c_get_exponents**

- NOTE: invert exponents to get negative exponents as they would be produced by linear regression

**d_plotLinearLoad**

Younger adults:
![image](figures/d_fooof_rcp_ya.png)

Older adults:
![image](figures/d_fooof_rcp_oa.png)

**e,f**

- these are not in use, but try to fit a combined model across rest and task spectra; note that rest and task spectra have different frequency definitions

**g01_taskPLS_load_YAOA_2group**

- only use posterior-occipital channels

Topo:
![image](figures/g01_pls_topo.png)

RCP:
![image](figures/g01_pls_rcp.png)

**h_plotSpectra**

Younger adults:
![image](figures/h_spectra_ya.png)

Older adults:
![image](figures/h_spectra_oa.png)
